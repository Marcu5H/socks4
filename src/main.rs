// Copyright (C) marc5h

// Implemented from https://www.openssh.com/txt/socks4.protocol

use std::net::SocketAddr;

use log::{self, error, info, warn};
use tokio::{
    io::{AsyncReadExt, AsyncWriteExt},
    net::{TcpListener, TcpSocket, TcpStream},
    select,
};

const REQUEST_GRANTED: u8 = 90;
const REQUEST_REJECTED_OR_FAILED: u8 = 91;
// const REQUEST_REJECTED_BECAUSE_SOCKS_SERVER_CANNOT_CONNECT_TO_INDENTD_ON_THE_CLIENT: u8 = 91;
// const REQUEST_REJECTED_BECAUSE_THE_CLIENT_PROGRAM_AND_IDENTD_REPORT_DIFFERENT_USER_IDS: u8 = 91;

/// Returns true if the `vn` is 4 (SOCKSv4)
fn vn_is_socks4(vn: u8) -> bool {
    return vn == 4;
}

async fn send_reply(socket: &mut TcpStream, reply: u8) {
    if let Err(e) = socket.write_all(&[0, reply]).await {
        error!("Error occured sending reply to client: {e}");
    }
}

async fn send_big_reply(socket: &mut TcpStream, reply: &[u8]) {
    if let Err(e) = socket.write_all(reply).await {
        error!("Error occured sending reply to client: {e}");
    }
}

async fn receive_user_id(socket: &mut TcpStream) -> Option<Vec<u8>> {
    let mut user_id: Vec<u8> = Vec::new();
    let mut buf: [u8; 1] = [0];
    loop {
        match socket.read(&mut buf).await {
            Ok(n) if n == 0 => {
                warn!("Missing data from client. Dropping");
                // TODO: Send reply
                return None;
            }
            Ok(_) => {
                if buf[0] == 0 {
                    break;
                }
                user_id.push(buf[0]);
            }
            Err(e) => {
                error!("Error occured reading UserID from client: {e}");
                // TODO: Send reply
                return None;
            }
        }
    }
    return Some(user_id);
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();

    let listener = TcpListener::bind("127.0.0.1:1337").await?;
    info!("Started tcp listener");

    loop {
        let (mut socket, _) = listener.accept().await?;
        info!("Client connected");

        tokio::spawn(async move {
            // First part of the CONNECT packet
            // VN (1) | CD (1) | DSTPORT (2) | DSTIP (4)
            let mut socks_packet = [0; 8];
            match socket.read(&mut socks_packet).await {
                Ok(n) if n < 8 => {
                    warn!("Received less than 8 bytes from client. Dropping");
                    return;
                }
                Ok(_) => {}
                Err(e) => {
                    error!("Error occured reading from client: {e}");
                    return;
                }
            }

            // Check if client connects to a SOCKSv4 server
            if !vn_is_socks4(socks_packet[0]) {
                warn!("VN is not 4");
                send_reply(&mut socket, REQUEST_REJECTED_OR_FAILED).await;
                return;
            }

            match socks_packet[1] {
                // CONNECT
                1 => {
                    info!("Received CONNECT request");
                    // Get the destination port number
                    let dst_port = ((socks_packet[2] as u16) << 8) + socks_packet[3] as u16;
                    // Create a destination IP address string
                    let dst_ip: String = format!(
                        "{}.{}.{}.{}",
                        socks_packet[4], socks_packet[5], socks_packet[6], socks_packet[7]
                    );
                    info!("Destination: {dst_ip}:{dst_port}");

                    let user_id = match receive_user_id(&mut socket).await {
                        Some(uid) => uid,
                        None => {
                            send_reply(&mut socket, REQUEST_REJECTED_OR_FAILED).await;
                            return;
                        }
                    };

                    info!("Received user id: {:?}", user_id);

                    let dst_addr: SocketAddr = match format!("{dst_ip}:{dst_port}").parse() {
                        Ok(a) => a,
                        Err(e) => {
                            error!("Error occured parsing destination address: {e}");
                            send_reply(&mut socket, REQUEST_REJECTED_OR_FAILED).await;
                            return;
                        }
                    };
                    let dst_socket = match TcpSocket::new_v4() {
                        Ok(s) => s,
                        Err(e) => {
                            error!("Error occured creating dst_socket: {e}");
                            send_reply(&mut socket, REQUEST_REJECTED_OR_FAILED).await;
                            return;
                        }
                    };
                    // Connect to the destination
                    let mut dst_stream = match dst_socket.connect(dst_addr).await {
                        Ok(s) => {
                            info!("Successfully connected to {dst_ip}:{dst_port}");
                            s
                        }
                        Err(e) => {
                            error!("Error occured creating dst_stream: {e}");
                            send_reply(&mut socket, REQUEST_REJECTED_OR_FAILED).await;
                            return;
                        }
                    };

                    send_big_reply(
                        &mut socket,
                        &[
                            0,
                            REQUEST_GRANTED,
                            socks_packet[2],
                            socks_packet[3],
                            socks_packet[4],
                            socks_packet[5],
                            socks_packet[6],
                            socks_packet[7],
                        ],
                    )
                    .await;
                    info!("Sent REQUEST_GRANTED reply");

                    // Create two buffers of 8KiB
                    let mut client_buf = [0; 1024 * 8];
                    let mut dst_buf = [0; 1024 * 8];
                    let (mut dst_reader, mut dst_writer) = dst_stream.split();
                    let (mut client_reader, mut client_writer) = socket.split();
                    loop {
                        select! {
                            res = client_reader.read(&mut client_buf) => {
                                match res {
                                    Ok(n) if n == 0 => {
                                        info!("Client disconnected");
                                        return;
                                    }
                                    Ok(n) => {
                                        info!("Received {n} bytes from client");
                                        match dst_writer.write_all(&client_buf[0..n]).await {
                                            Err(e) => {
                                                error!("Error occured sending data to dst: {e}");
                                                return;
                                            }
                                            _ => {}
                                        }
                                    }
                                    Err(e) => {
                                        error!("Error occured reading from client: {e}");
                                        return;
                                    }
                                }
                            },
                            res = dst_reader.read(&mut dst_buf) => {
                                match res {
                                    Ok(n) if n == 0 => {
                                        info!("Destination disconnected");
                                        return;
                                    }
                                    Ok(n) => {
                                        info!("Received {n} bytes from dst");
                                        match client_writer.write_all(&dst_buf[0..n]).await {
                                            Err(e) => {
                                                error!("Error occured sending data to client: {e}");
                                                return;
                                            }
                                            _ => {}
                                        }
                                    }
                                    Err(e) => {
                                        error!("Error occured reading from dst: {e}");
                                        return;
                                    }
                                }
                            },
                        }
                    }
                }
                // TODO:
                // BIND 
                // 2 => {
                // }
                _ => {
                    warn!("Undefined command code recieved: {}", socks_packet[1]);
                    send_reply(&mut socket, REQUEST_REJECTED_OR_FAILED).await;
                    return;
                }
            }
        });
    }
}
