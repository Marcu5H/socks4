## Runnnig

Download the repository.

```console
$ RUST_LOG=info cargo run --release # remove or change `RUST_LOG` to the log levels you want
```

Now the server should run on `127.0.0.1:1337`. To test the connection execute the following command.
```console
$ curl --proxy "socks4://127.0.0.1:1337" "https://gitlab.com" -v
```
